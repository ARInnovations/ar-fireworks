import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SafetyComponent } from './safety/safety.component';
import { safetyRoutingModule } from './safety-routing';

@NgModule({
  imports: [
    CommonModule,safetyRoutingModule
  ],
  declarations: [SafetyComponent]
})
export class SafetyModule { }

import {RouterModule, Routes} from '@angular/router';
import {ModuleWithProviders} from '@angular/core';
import { SafetyComponent } from './safety/safety.component';


const safetyRoutes: Routes = [
{path:'details',component:SafetyComponent},
{path:'', redirectTo:'details',pathMatch:'full'}
];

export const safetyRoutingModule: ModuleWithProviders = RouterModule.forChild(safetyRoutes);

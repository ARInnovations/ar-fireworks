import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { HomeRoutingModule } from './home-routing';
import { HttpClientModule } from '@angular/common/http';
import { OwlModule } from 'ngx-owl-carousel';

import {  ReactiveFormsModule } from '@angular/forms';

import { HttpModule } from '@angular/http';

import { Ng2DeviceDetectorModule } from 'ng2-device-detector';



@NgModule({
  imports: [
    CommonModule,HomeRoutingModule,OwlModule,HttpClientModule,ReactiveFormsModule,HttpModule,Ng2DeviceDetectorModule.forRoot()
  ],
  declarations: [HomeComponent]
})
export class HomeModule {
}

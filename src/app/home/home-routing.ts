import {RouterModule, Routes} from '@angular/router';
import {ModuleWithProviders} from '@angular/core';
import { HomeComponent } from './home/home.component';


const submissionRoutes: Routes = [
{path:'home',component:HomeComponent}
];

export const HomeRoutingModule: ModuleWithProviders = RouterModule.forChild(submissionRoutes);

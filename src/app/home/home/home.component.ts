import { Component, OnInit,AfterViewInit,ViewChild,ViewEncapsulation } from '@angular/core';


import { FormGroup, FormControl, Validators ,ReactiveFormsModule  } from '@angular/forms';

import { Http, Response, Headers, RequestOptions,HttpModule  } from '@angular/http';
import { map } from 'rxjs/operators';


import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';


import { URLSearchParams } from '@angular/http';
import {Observable} from 'rxjs';
import { Ng2DeviceService } from 'ng2-device-detector';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent{


  deviceInfo = null;
  showModal:boolean=true;
  visName:any;
  visMobile:any;
  url:any;
  val:any;
  constructor(private httpClient: HttpClient,private deviceService: Ng2DeviceService)
  {
    if(localStorage.getItem('alreadyVisited'))
      this.showModal=false;
    this.deviceInfo = this.deviceService.getDeviceInfo();
    if(this.deviceInfo.device=="android" || this.deviceInfo.device=="iphone")
    {
      alert("Please View the Website in landspace mode for better visiblity");
    }

  }

  visitorForm = new FormGroup({
    visitorName: new FormControl('',[Validators.required,Validators.minLength(3)]),
    visitorMobile: new FormControl('',[Validators.required,Validators.pattern('^[6|7|8|9|0][0-9]{9,11}')]),
  });

submitVisitor()
{
  this.showModal=false;
  localStorage.setItem('alreadyVisited', '1');
  this.visName=this.visitorForm.get('visitorName').value;
  this.visMobile=this.visitorForm.get('visitorMobile').value;
  this.val="?name="+this.visName+"&mobile="+this.visMobile;
  this.url='https://script.google.com/macros/s/AKfycbyxPE9z_pP3TdbWx2w5JLWM0Pq1unbjBKIo6-KpAMXMnShjTw/exec';
  this.url+=this.val;
  this.httpClient.get(this.url).pipe(map(data => {})).subscribe(result => {
  });
}

sendEmail() {
  let url = 'https://us-central1-ar-fireworks-d5a8e.cloudfunctions.net/sendEmailConfirmation';

      let emailHeaders = new HttpHeaders();
      emailHeaders.append('Content-Type', 'application/json');
      emailHeaders.append('Access-Control-Allow-Origin', '*');

      let emailParams = new HttpParams();
      emailParams.append('to', 'arfireworks@gmail.com');
      emailParams.append('from', 'arfireworks@gmail.com'); /** replace 'example@gmail.com' with a working email address, I simply sent an email to myself. */
      emailParams.append('subject', 'Test 1 Subject');
      emailParams.append('text', 'test 1 text');
      emailParams.append('html', '<html><p>Test 1!</p></html>');

      let body = {
        to: 'arfireworks@gmail.com',
        from: 'arfireworks@gmail.com',
        subject: 'Test 1 Subject',
        text: 'test 1 text',
        html: '<html><p>Test 1!</p></html>'
      };

      return this.httpClient.post(url, body, {
          headers: emailHeaders,
          params: emailParams
        })
        .toPromise()
        .then(res => {
          console.log(res);
        })
        .catch(err => {
          console.log(err);
        });

    }





}

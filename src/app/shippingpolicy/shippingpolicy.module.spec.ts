import { ShippingpolicyModule } from './shippingpolicy.module';

describe('ShippingpolicyModule', () => {
  let shippingpolicyModule: ShippingpolicyModule;

  beforeEach(() => {
    shippingpolicyModule = new ShippingpolicyModule();
  });

  it('should create an instance', () => {
    expect(shippingpolicyModule).toBeTruthy();
  });
});

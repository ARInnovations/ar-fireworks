import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ShippingComponent } from './shipping/shipping.component';

import { ShippingRoutingModule } from './shipping-routing';

@NgModule({
  imports: [
    CommonModule,ShippingRoutingModule
  ],
  declarations: [ShippingComponent]
})
export class ShippingpolicyModule { }

import {RouterModule, Routes} from '@angular/router';
import {ModuleWithProviders} from '@angular/core';
import { ShippingComponent } from './shipping/shipping.component';



const shippingRoutes: Routes = [
{path:'details',component:ShippingComponent},
{path:'', redirectTo:'details',pathMatch:'full'}
];

export const ShippingRoutingModule: ModuleWithProviders = RouterModule.forChild(shippingRoutes);

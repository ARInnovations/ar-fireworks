import {RouterModule, Routes} from '@angular/router';
import {ModuleWithProviders} from '@angular/core';
import { DetailsComponent } from './details/details.component';


const paymentRoutes: Routes = [
{path:'details',component:DetailsComponent},
{path:'', redirectTo:'details',pathMatch:'full'}
];

export const paymentRoutingModule: ModuleWithProviders = RouterModule.forChild(paymentRoutes);

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DetailsComponent } from './details/details.component';
import { paymentRoutingModule } from './payment-routing';

@NgModule({
  imports: [
    CommonModule,paymentRoutingModule
  ],
  declarations: [DetailsComponent]
})
export class PaymentModule { }

import { Component, OnInit } from '@angular/core';
import { NavBarService } from '../nav-bar.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit{
  applyrouteColor:boolean;
  constructor(public nav: NavBarService,private activeRoute:ActivatedRoute,location: Location,router:Router)
  {
    router.events.subscribe((val) => {
      if(location.path().indexOf('/order/') !== -1)
      {
        this.applyrouteColor=true;
      }
      else
      {
        this.applyrouteColor=false;
      }
    });
  }

  ngOnInit() {


}


}

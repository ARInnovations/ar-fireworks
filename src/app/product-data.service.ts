import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ProductDataService {

  constructor() {  }
  showNav=true;
  
  orderValue=[{"orderValue":0,"orderedProduct":0}];
 products = [
    {
      "productCode": "header",
      "productName": "CHAKKARS(10 pieces)"
    },
    {
      "productCode": "1",
      "productName": "Ground spinner Big (25pieces)",
      "rate": 100,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "2",
      "productName": "Ground spinner Super",
      "rate": 42,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "3",
      "productName": "Ground spinner Special",
      "rate": 80,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "4",
      "productName": "Ground spinner Deluxe",
      "rate": 130,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "header",
      "productName": "FLOWER POTS(10 pieces)",
   
    },
    {
      "productCode": "5",
      "productName": "Flower Pots Small ",
      "rate": 60,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "6",
      "productName": "Flower Pots Big",
      "rate": 80,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "7",
      "productName": "Flower Pots Special",
      "rate": 100,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "8",
      "productName": "Flower Pots Giant ",
      "rate": 160,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "9",
      "productName": "Colour Koti-10s",
      "rate": 160,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "10",
      "productName": "Colour Koti-5s",
      "rate": 86,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "11",
      "productName": "Rangeela",
      "rate": 280,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "12",
      "productName": "Flower Pots Deluxe",
      "rate": 260,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "header",
      "productName": "FANCIES/FOUNTAINS"
    },
    {
      "productCode": "15",
      "productName": "Siren",
      "rate": 320,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "16",
      "productName": "Peacock Dance",
      "rate": 300,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "17",
      "productName": "Green Glitterings",
      "rate": 300,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "18",
      "productName": "Red Glitterings",
      "rate": 300,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "19",
      "productName": "WhiteGlitterings",
      "rate": 300,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "20",
      "productName": "Star Glitterings",
      "rate": 300,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "21",
      "productName": "Butterfly",
      "rate": 160,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "22",
      "productName": "Sizzling stars",
      "rate": 420,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "23",
      "productName": "Mangalyan ",
      "rate": 120,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "24",
      "productName": "Jurrasic Park",
      "rate": 40,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "25",
      "productName": "Battle City",
      "rate": 40,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "26",
      "productName": "Salsa (4 pieces)",
      "rate": 160,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "header",
      "productName": "TWINKLING STAR"
    },
    {
      "productCode": "27",
      "productName": "1.5\" Twinkling star",
      "rate": 36,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "28",
      "productName": "Deluxe Twinkling star",
      "rate": 80,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "header",
      "productName": "PENCIL"
    },
    {
      "productCode": "29",
      "productName": "Fire Pencil",
      "rate": 36,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "30",
      "productName": "Coronation Candle",
      "rate": 80,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "header",
      "productName": "CARTOONS",
    },
    {
      "productCode": "31",
      "productName": "Assorted Cartoon",
      "rate": 29,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "32",
      "productName": "Diamond Flash",
      "rate": 21,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "header",
      "productName": "ATOM BOMBS (10PIECES)"
    },
    {
      "productCode": "33",
      "productName": "Queen of Queens",
      "rate": 74,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "34",
      "productName": "King of king",
      "rate": 120,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "35",
      "productName": "Delux bomb ",
      "rate": 160,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "36",
      "productName": "DTS bomb",
      "rate": 170,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "37",
      "productName": "Bullet Bomb`",
      "rate": 24,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "header",
      "productName": "ONE SOUND CRACKERS",
    },
    {
      "productCode": "38",
      "productName": "4\" Lakshmi ",
      "rate": 26,
      "ratePer": "1 Packet",
      "requirement": 0
    },
    {
      "productCode": "39",
      "productName": "2.75\" Kuruvi",
      "rate": 12,
      "ratePer": "1 Packet",
      "requirement": 0
    },
    {
      "productCode": "40",
      "productName": "4\" Rabbit Deluxe",
      "rate": 40,
      "ratePer": "1 Packet",
      "requirement": 0
    },
    {
      "productCode": "41",
      "productName": "Double Sound ",
      "rate": 32,
      "ratePer": "1 Packet",
      "requirement": 0
    },
    {
      "productCode": "header",
      "productName": "BIJILI CRACKERS"
    },
    {
      "productCode": "42",
      "productName": "Bijili Red(100 pcs)",
      "rate": 32,
      "ratePer": "1 Packet",
      "requirement": 0
    },
    {
      "productCode": "43",
      "productName": "Bijili Stripped(100 pcs)",
      "rate": 34,
      "ratePer": "1 Packet",
      "requirement": 0
    },
    {
      "productCode": "header",
      "productName": "CHORSA & GIANT CRACKERS",
    },
    {
      "productCode": "44",
      "productName": "20 chorsa ",
      "rate": 14,
      "ratePer": "1 Packet",
      "requirement": 0
    },
    {
      "productCode": "45",
      "productName": "28 Chorsa/Goa",
      "rate": 15,
      "ratePer": "1 Packet",
      "requirement": 0
    },
    {
      "productCode": "46",
      "productName": "28 Giant",
      "rate": 28,
      "ratePer": "1 Packet",
      "requirement": 0
    },
    {
      "productCode": "47",
      "productName": "56 Giant ",
      "rate": 56,
      "ratePer": "1 Packet",
      "requirement": 0
    },
    {
      "productCode": "48",
      "productName": "56 wala giant",
      "rate": 58,
      "ratePer": "1 Packet",
      "requirement": 0
    },
    {
      "productCode": "header",
      "productName": "DELUXE CRACKERS",
    },
    {
      "productCode": "49",
      "productName": "20 deluxe",
      "rate": 48,
      "ratePer": "1 Packet",
      "requirement": 0
    },
    {
      "productCode": "50",
      "productName": "24 deluxe",
      "rate": 56,
      "ratePer": "1 Packet",
      "requirement": 0
    },
    {
      "productCode": "51",
      "productName": "50 deluxe",
      "rate": 120,
      "ratePer": "1 Packet",
      "requirement": 0
    },
    {
      "productCode": "52",
      "productName": "100 deluxe",
      "rate": 240,
      "ratePer": "1 Packet",
      "requirement": 0
    },
    {
      "productCode": "header",
      "productName": "GARLANDS"
    },
    {
      "productCode": "53",
      "productName": "100 wala",
      "rate": 58,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "54",
      "productName": "200 wala",
      "rate": 116,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "55",
      "productName": "300 wala",
      "rate": 160,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "56",
      "productName": "600 wala",
      "rate": 320,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "57",
      "productName": "1000 wala",
      "rate": 400,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "58",
      "productName": "2000 wala",
      "rate": 800,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "59",
      "productName": "5000 wala",
      "rate": 2000,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "60",
      "productName": "10000 wala",
      "rate": 4000,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "header",
      "productName": "ROCKETS"
    },
    {
      "productCode": "61",
      "productName": "Baby Rocket",
      "rate": 36,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "62",
      "productName": "Rocket Bomb",
      "rate": 60,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "63",
      "productName": "Crackling Rocket",
      "rate": 168,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "64",
      "productName": "Whistling Rocket",
      "rate": 190,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "header",
      "productName": "FANCY NOVELTIES "
    },
    {
      "productCode": "65",
      "productName": "Kit Kat",
      "rate": 49,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "66",
      "productName": "Dancing Butterfly",
      "rate": 147,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "67",
      "productName": "Crunch Pop (2Pcs)(Crackling)",
      "rate": 237,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "68",
      "productName": "Hot Wheels( 5 Pcs)",
      "rate": 210,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "69",
      "productName": "Spider Man(15 Pcs) (Crackling)",
      "rate": 423,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "70",
      "productName": "Super Heroes(15 Pcs) (Crackling)",
      "rate": 423,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "71",
      "productName": "Hulk (15 Pcs) (Crackling)",
      "rate": 423,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "72",
      "productName": "Five Star(2 Pcs)",
      "rate": 77,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "73",
      "productName": "Pyro Park (2 Pcs)",
      "rate": 95,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "74",
      "productName": "Fancy Week (2 Pcs)",
      "rate": 196,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "header",
      "productName": "FLASH MOUNTAINS (2.5\" Diameter)"
    },
    {
      "productCode": "75",
      "productName": "Bubble Boo(5 Pcs)",
      "rate": 161,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "76",
      "productName": "Candy Rush (5 Pcs)",
      "rate": 161,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "77",
      "productName": "Chubby Tubbies(5 Pcs)",
      "rate": 161,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "78",
      "productName": "Diamond Digger(5 Pcs)",
      "rate": 161,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "79",
      "productName": "Scrubby Dubby (5 Pcs)",
      "rate": 161,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "80",
      "productName": "Niagara Falls(5 Pcs)",
      "rate": 230,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "81",
      "productName": "Brindavan (5 Pcs)",
      "rate": 230,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "82",
      "productName": "Silver Cascade ( 5 Pcs)",
      "rate": 230,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "83",
      "productName": "I Love India (3 Pcs)",
      "rate": 251.85,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "84",
      "productName": "Desi Rang(3 Pcs)",
      "rate": 251.85,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "85",
      "productName": "Golden Peacock",
      "rate": 209.3,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "86",
      "productName": "Mora( Crackling)",
      "rate": 209.3,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "87",
      "productName": "Rang Mayur",
      "rate": 209.3,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "header",
      "productName": "FANCY WHEELS"
    },
    {
      "productCode": "88",
      "productName": "X-Man (3Pcs) ",
      "rate": 118,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "89",
      "productName": "Angry Birds (5 Pcs)",
      "rate": 154,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "90",
      "productName": "Moana (3 Pcs)",
      "rate": 132,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "header",
      "productName": "MULTIPLE MULTI COLOURS (WITH CRACKLING)",
    },
    {
      "productCode": "91",
      "productName": "Star Night 5",
      "rate": 148,
      "ratePer": "1piece",
      "requirement": 0
    },
    {
      "productCode": "92",
      "productName": "Holiday Fun 10 shot",
      "rate": 242,
      "ratePer": "1piece",
      "requirement": 0
    },
    {
      "productCode": "93",
      "productName": "Enjoy 15 shot",
      "rate": 377,
      "ratePer": "1piece",
      "requirement": 0
    },
    {
      "productCode": "94",
      "productName": "Good day 30 shot",
      "rate": 656,
      "ratePer": "1piece",
      "requirement": 0
    },
    {
      "productCode": "95",
      "productName": "Symphony 50 shot",
      "rate": 787,
      "ratePer": "1piece",
      "requirement": 0
    },
    {
      "productCode": "96",
      "productName": "Maza Maza 60 shot",
      "rate": 1259,
      "ratePer": "1piece",
      "requirement": 0
    },
    {
      "productCode": "header",
      "productName": "Rang Park(2\" Diameter)(1pcs each)"
    },
    {
      "productCode": "97",
      "productName": "Earth",
      "rate": 165.6,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "98",
      "productName": "Air",
      "rate": 165.6,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "99",
      "productName": "Fire",
      "rate": 165.6,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "100",
      "productName": "Sky/Water",
      "rate": 165.6,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "101",
      "productName": "IPL series ",
      "rate": 368,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "header",
      "productName": "Rang dhara (3 in 1 crackling) (3pcs)"
    },
    {
      "productCode": "201",
      "productName": "Reng be ranga (3 pcs)",
      "rate": 598,
      "ratePer": "1 Box",
      "requirement": 0
    },
        {
              "productCode": "202",
                    "productName": "Dhoom",
                          "rate": 598,
                                "ratePer": "1 Box",
                                      "requirement": 0
                                          },
                                              {
      "productCode": "203",
      "productName": "Dhilrupa",
      "rate": 598,
      "ratePer": "1 Box",
      "requirement": 0
    },
        {
      "productCode": "204",
      "productName": "Golmaal",
      "rate": 598,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "header",
      "productName": "Rang Flora (Crackling)(3\"Diameter)(1pcs each)"
    },
    {
      "productCode": "102",
      "productName": "Jade",
      "rate": 242,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "103",
      "productName": "Hazal",
      "rate": 242,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "104",
      "productName": "Daisy",
      "rate": 242,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "105",
      "productName": "Lola",
      "rate": 242,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "106",
      "productName": "Aurora",
      "rate": 242,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "107",
      "productName": "Elina",
      "rate": 242,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "header",
      "productName": "Rang Music (3 in 1 crackling)(3pcs each)(3\"Diameter)",
    },
    {
      "productCode": "108",
      "productName": "Har Monika (3Pcs)",
      "rate": 685,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "109",
      "productName": "Drums(3Pcs)",
      "rate": 685,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "110",
      "productName": "Guitar (3Pcs)",
      "rate": 685,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "111",
      "productName": "Tabla(3 Pcs)",
      "rate": 685,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "112",
      "productName": "Violin(3Pcs)",
      "rate": 685,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "header",
      "productName": "Rang Dances 16 items"
    },
    {
      "productCode": "200",
      "productName": "Salsa",
      "rate": 407,
      "ratePer": "1 Piece",
      "requirement": 0
    },
    {
      "productCode": "113",
      "productName": "Break",
      "rate": 407,
      "ratePer": "1 Piece",
      "requirement": 0
    },
    {
      "productCode": "114",
      "productName": "Hip-Hop",
      "rate": 407,
      "ratePer": "1 Piece",
      "requirement": 0
    },
    {
      "productCode": "115",
      "productName": "Manipuri",
      "rate": 407,
      "ratePer": "1 Piece",
      "requirement": 0
    },
    {
      "productCode": "116",
      "productName": "Ace(3pcs)",
      "rate": 1075,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "117",
      "productName": "King (3pcs)",
      "rate": 1075,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "118",
      "productName": "Queen(3pcs)",
      "rate": 1075,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "119",
      "productName": "Jack(3pcs)",
      "rate": 1075,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "120",
      "productName": "Joker (3pcs)",
      "rate": 1075,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "header",
      "productName": "New Variety Sparklers"
    },
    {
      "productCode": "121",
      "productName": "12 cm Twin Tone",
      "rate": 54,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "122",
      "productName": "15cm Cocktail",
      "rate": 85,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "123",
      "productName": "Lovely Sparklers",
      "rate": 95,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "header",
      "productName": "SPARKLES"
    },
    {
      "productCode": "124",
      "productName": "7 cm Electric",
      "rate": 10,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "125",
      "productName": "7 cm Crackling",
      "rate": 12,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "126",
      "productName": "10cm Electric",
      "rate": 26,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "127",
      "productName": "10 cm Crackling",
      "rate": 30,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "128",
      "productName": "10 cm Green",
      "rate": 30,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "129",
      "productName": "10 cm Red",
      "rate": 36,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "130",
      "productName": "10 cm Star Plus",
      "rate": 36,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "131",
      "productName": "10 cm Japan",
      "rate": 36,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "132",
      "productName": "10 cm gliteering colors",
      "rate": 38,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "133",
      "productName": "12 cm Electric",
      "rate": 38,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "134",
      "productName": "12 cm Crackling",
      "rate": 42,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "135",
      "productName": "15 cm Electric",
      "rate": 60,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "136",
      "productName": "15 cm Crackling",
      "rate": 70,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "137",
      "productName": "15 cm Green",
      "rate": 70,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "138",
      "productName": "15 cm Red",
      "rate": 80,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "139",
      "productName": "30 cm Electric",
      "rate": 60,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "140",
      "productName": "30 cm Crackling",
      "rate": 70,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "141",
      "productName": "30 cm Green",
      "rate": 70,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "142",
      "productName": "30 cm Red",
      "rate": 80,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "143",
      "productName": "30 cm 5 in 1",
      "rate": 80,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "144",
      "productName": "50 cm Electric",
      "rate": 180,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "145",
      "productName": "50 cm Crackling",
      "rate": 240,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "146",
      "productName": "50 cm 5 in 1",
      "rate": 260,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "147",
      "productName": "75 cm mix",
      "rate": 320,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "header",
      "productName": "AERIAL FANCIES"
    },
    {
      "productCode": "148",
      "productName": "7 Wonders ",
      "rate": 240,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "149",
      "productName": "Twelve shots",
      "rate": 200,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "150",
      "productName": "Rainbow",
      "rate": 260,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "151",
      "productName": "25 shot",
      "rate": 420,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "152",
      "productName": "30 shot",
      "rate": 720,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "153",
      "productName": "50 shot",
      "rate": 840,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "154",
      "productName": "60 shot",
      "rate": 1400,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "155",
      "productName": "100 shot",
      "rate": 1950,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "156",
      "productName": "100 shot Glittering",
      "rate": 3100,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "157",
      "productName": "120 shot",
      "rate": 2300,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "158",
      "productName": "240 shot",
      "rate": 4400,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "159",
      "productName": "chotta fancy",
      "rate": 80,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "160",
      "productName": "2\" fancy",
      "rate": 220,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "161",
      "productName": "2.5\" fancy",
      "rate": 260,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "162",
      "productName": "3\" fancy",
      "rate": 340,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "163",
      "productName": "3.5\" fancy",
      "rate": 420,
      "ratePer": "1 Box",
      "requirement": 0
    },
    {
      "productCode": "164",
      "productName": "Mega Fancy(2 pcs)",
      "rate": 900,
      "ratePer": "1 Box",
      "requirement": 0
    }
   ]
 products2 = [
  {
    "productCode": "1",
    "productName": "The warm House",
    "rate": 420,
    "ratePer": "1 Box",
    "requirement": 0
  },
  {
    "productCode": "2",
    "productName": "Gifts of Love",
    "rate": 530,
    "ratePer": "1 Box",
    "requirement": 0
  },
  {
    "productCode": "3",
    "productName": "High Five",
    "rate": 735,
    "ratePer": "1 Box",
    "requirement": 0
  },
  {
    "productCode": "4",
    "productName": "Heart of Gold",
    "rate": 935,
    "ratePer": "1 Box",
    "requirement": 0
  },
   {
    "productCode": "5",
    "productName": "The treasure Box",
    "rate": 1245,
    "ratePer": "1 Box",
    "requirement": 0
  },
   {
    "productCode": "6",
    "productName": " Surprises",
    "rate": 1560,
    "ratePer": "1 Box",
    "requirement": 0
  }
 ]
 

}

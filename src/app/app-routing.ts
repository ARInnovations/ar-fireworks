import {RouterModule, Routes} from '@angular/router';
import {ModuleWithProviders} from '@angular/core';

import { AppComponent } from './app.component';
import { PreloadAllModules } from '@angular/router';


const appRoutes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
{path:'order', loadChildren:'./order/order.module#OrderModule'},
{path:'home', loadChildren:'./home/home.module#HomeModule'},
{path:'safety',loadChildren:'./safety/safety.module#SafetyModule'},
{path:'shipping',loadChildren:'./shippingpolicy/shippingpolicy.module#ShippingpolicyModule'},
{path:'contact',loadChildren:'./contact/contact.module#ContactModule'},
{path:'view-orders',loadChildren:'./view-orders/view-orders.module#ViewOrdersModule'},
{path:'payment',loadChildren:'./payment/payment.module#PaymentModule'}

];

export const AppRoutingModule: ModuleWithProviders = RouterModule.forRoot(appRoutes,{ useHash: true,preloadingStrategy: PreloadAllModules });

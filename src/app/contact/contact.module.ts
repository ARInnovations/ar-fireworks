import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContactComponent } from './contact/contact.component';

import { contactRoutingModule } from './contact-routing';

@NgModule({
  imports: [
    CommonModule,contactRoutingModule
  ],
  declarations: [ContactComponent]
})
export class ContactModule { }

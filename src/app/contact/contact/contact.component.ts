import { Component, OnInit,ViewChild } from '@angular/core';
import { } from '@types/googlemaps';

@Component({
  selector: 'app-contactus',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {
  @ViewChild('googleMap') gmapElement: any;
  map: google.maps.Map;
  constructor() { }

  ngOnInit() {
    var mapProp = {
 center: new google.maps.LatLng(9.444188,77.796313),
 zoom: 15,
  mapTypeId: google.maps.MapTypeId.TERRAIN
 };

 this.map = new google.maps.Map(this.gmapElement.nativeElement, mapProp);
 var marker = new google.maps.Marker({ position: mapProp.center });
 marker.setMap(this.map);


  }

}

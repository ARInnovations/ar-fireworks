import {RouterModule, Routes} from '@angular/router';
import {ModuleWithProviders} from '@angular/core';
import { ContactComponent } from './contact/contact.component';


const contactRoutes: Routes = [
{path:'details',component:ContactComponent},
{path:'', redirectTo:'details',pathMatch:'full'}
];

export const contactRoutingModule: ModuleWithProviders = RouterModule.forChild(contactRoutes);

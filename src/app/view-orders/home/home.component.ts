import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { NavBarService } from '../../nav-bar.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  individualOrder:any;
  constructor(private http:HttpClient,public nav: NavBarService)
   {
     this.http.get('https://spreadsheets.google.com/feeds/list/1vabZau1P2XsCeAmtUsK1HCDlXJH7lKdWeXL6XcSgMVM/od6/public/values?alt=json').pipe(map(data => {
       this.individualOrder=data;
     })).subscribe(result => {});

     nav.hide();


   }





  ngOnInit() {
  }

}

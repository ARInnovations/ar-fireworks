import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListComponent } from './list/list.component';

import { ViewOrdersRoutingModule } from './view-orders-routing';
import { HttpClientModule } from '@angular/common/http';
import { DetailsComponent } from './details/details.component';
import { HomeComponent } from './home/home.component';
import { VisitorsComponent } from './visitors/visitors.component';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './auth-guard.service';
import { ReactiveFormsModule }    from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,ViewOrdersRoutingModule,HttpClientModule,ReactiveFormsModule
  ],
  providers:[AuthGuard],
  declarations: [ListComponent, DetailsComponent, HomeComponent, VisitorsComponent, LoginComponent]
})
export class ViewOrdersModule { }

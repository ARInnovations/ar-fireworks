import { Component, OnInit } from '@angular/core';
import { NavBarService } from '../../nav-bar.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;

  constructor(
      private formBuilder: FormBuilder,
      private route: ActivatedRoute,
      private router: Router,
      public nav: NavBarService) {
        nav.hide();
      }

  ngOnInit() {
      this.loginForm = this.formBuilder.group({
          username: ['', Validators.required],
          password: ['', Validators.required]
      });

  }

  onSubmit() {
      
      // stop here if form is invalid
      if (this.loginForm.invalid) {
          return;
      }
      else {
        if(this.loginForm.get('username').value === "arfireworks@gmail.com" && this.loginForm.get('password').value === "arfireworks@1") {
          this.nav.isAuthenticated = true;
          this.router.navigateByUrl('view-orders/home');
        }
      }
     
  }
}

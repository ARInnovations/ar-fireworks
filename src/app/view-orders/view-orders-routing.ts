import {RouterModule, Routes} from '@angular/router';
import {ModuleWithProviders} from '@angular/core';
import { ListComponent } from './list/list.component';
import { DetailsComponent } from './details/details.component';
import { HomeComponent } from './home/home.component';
import { VisitorsComponent } from './visitors/visitors.component';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from "./auth-guard.service";



const viewOrdersRoutes: Routes = [
{path:'login',component:LoginComponent},    
{
    path:'home',
    component:HomeComponent,
    canActivate: [AuthGuard],

},
{
    path:'list',
    component:ListComponent,
    canActivate: [AuthGuard],

},
{
    path:'visitors',
    component:VisitorsComponent,
    canActivate: [AuthGuard],

},
{
    path:'details/:product/:id',
    component:DetailsComponent,
    canActivate: [AuthGuard],

},
{path:'', redirectTo:'home',pathMatch:'full'}
];

export const ViewOrdersRoutingModule: ModuleWithProviders = RouterModule.forChild(viewOrdersRoutes);

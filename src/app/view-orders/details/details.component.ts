import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { ProductDataService } from '../../product-data.service';



@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {
  id:any;
  product:any;
  individualOrder:any;
  showLoader:boolean=true;
    orderDetails:any;
    productFlag:boolean;
  url:any;
  constructor(private route: ActivatedRoute,private http:HttpClient,private orderData:ProductDataService)
   {

     this.route.params.subscribe(res =>{
       this.id=res.id;
       this.product=res.product;

     });

     this.id=this.id-1;

     if(this.product==1)
     {
       this.orderDetails=orderData.products;

       let index = 1;
    for(let i = 0; i < this.orderDetails.length; i++)
    {
      if(this.orderDetails[i].productCode!='header')
        this.orderDetails[i].index=index++;
    }

    
       this.productFlag=true;
       this.url='https://spreadsheets.google.com/feeds/list/1vabZau1P2XsCeAmtUsK1HCDlXJH7lKdWeXL6XcSgMVM/od6/public/values?alt=json';
     }
     else
     {
       this.productFlag=false;
       this.orderDetails=orderData.products2;
       
   
       this.url='https://spreadsheets.google.com/feeds/list/1ZXHoLUzCtzf-UP4zkAOIQTPsPNSochOA5B6Y7wHQoNs/od6/public/values?alt=json';
     }

     this.http.get(this.url).pipe(map(data => {

       this.individualOrder=this.prettifyGoogleSheetsJSON(data,this.id);

       let index = 1;
       for(let i = 0; i < this.orderDetails.length; i++)
       {
         if(this.orderDetails[i].productCode!='header')
           this.orderDetails[i].index=index++;
       }

       this.showLoader=false;
     })).subscribe(result => {});
   }
   prettifyGoogleSheetsJSON(data,id)
   {
   for (var i = 0; i < data.feed.entry.length; i++)
   {
       for (var key in data.feed.entry[i])
       {
           if (data.feed.entry[i].hasOwnProperty(key) && key.substr(0,4) === 'gsx$')
           {

               data.feed.entry[i][key.substr(4)] = data.feed.entry[i][key].$t;
               delete data.feed.entry[i][key];
           }
       }
       delete data.feed.entry[i].id;
       delete data.feed.entry[i].updated;
       delete data.feed.entry[i].category;
       delete data.feed.entry[i].content;
       delete data.feed.entry[i].link;
       delete data.feed.entry[i].title;
   }

   return data.feed.entry[id];
 }
   getTotal()
   {
     let sum = 0;
     for(let i = 0; i < this.orderDetails.length; i++)
     {
       sum += this.individualOrder["p"+(i+1)]*this.orderDetails[i].rate;
     }

   return sum;
   }
  ngOnInit() {
  }
  tableToExcel (table,filename){
   let uri = 'data:application/vnd.ms-excel;base64,'
       , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--><meta http-equiv="content-type" content="text/plain; charset=UTF-8"/></head><body><table>{table}</table></body></html>'
       , base64 = function(s) { return window.btoa(decodeURIComponent(encodeURIComponent(s))) }
       , format = function(s,c) { return s.replace(/{(\w+)}/g, function(m, p) { return c[p]; }) }
           if (!table.nodeType) table = document.getElementById(table)
           var ctx = {worksheet: name || 'Worksheet', table: table.innerHTML}
           var link = document.createElement("a");
     link.download = "AR Fireworks Order "+this.individualOrder.name+".xls";
     link.href = uri + base64(format(template, ctx));
     link.click();
       }

}

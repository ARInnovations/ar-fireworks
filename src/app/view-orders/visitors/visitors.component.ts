import { Component, OnInit,Pipe, PipeTransform } from '@angular/core';
import { HttpClient } from "@angular/common/http";

import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-visitors',
  templateUrl: './visitors.component.html',
  styleUrls: ['./visitors.component.css']
})
export class VisitorsComponent {

  visitorsDetails:any;
  finalList=[]
  showLoader:boolean=true;
  constructor(private http:HttpClient)
  {

    this.http.get('https://spreadsheets.google.com/feeds/list/15vVci5hHyM2fpeWfrJQg9A3bJQF-YwZ3cHTXPCGrXxY/od6/public/values?alt=json').pipe(map(data => {
      this.visitorsDetails=this.prettifyGoogleSheetsJSON(data);
      this.showLoader=false;
    })).subscribe(result => {

    });






    }


    prettifyGoogleSheetsJSON(data)
    {
    for (var i = 0; i < data.feed.entry.length; i++)
    {
        for (var key in data.feed.entry[i])
        {
            if (data.feed.entry[i].hasOwnProperty(key) && key.substr(0,4) === 'gsx$')
            {

                data.feed.entry[i][key.substr(4)] = data.feed.entry[i][key].$t;
                delete data.feed.entry[i][key];
            }
        }
        delete data.feed.entry[i].id;
        delete data.feed.entry[i].updated;
        delete data.feed.entry[i].category;
        delete data.feed.entry[i].content;
        delete data.feed.entry[i].link;
        delete data.feed.entry[i].title;
    }

    return data.feed.entry;
  }

}

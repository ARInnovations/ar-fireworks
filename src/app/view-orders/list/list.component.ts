import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";

import { map } from 'rxjs/operators';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  ordersDetails:any;
  showLoader:boolean=true;
  ordersDetails1:any;
  showLoader1:boolean=true;
  constructor(private http:HttpClient)
  {

    this.http.get('https://spreadsheets.google.com/feeds/list/1vabZau1P2XsCeAmtUsK1HCDlXJH7lKdWeXL6XcSgMVM/od6/public/values?alt=json').pipe(map(data => {
    //  this.ordersDetails=data;
      this.ordersDetails=this.prettifyGoogleSheetsJSON(data);
      this.showLoader=false;
    })).subscribe(result => {

    });

    this.http.get('https://spreadsheets.google.com/feeds/list/1ZXHoLUzCtzf-UP4zkAOIQTPsPNSochOA5B6Y7wHQoNs/od6/public/values?alt=json').pipe(map(data => {
    //  this.ordersDetails=data;
      this.ordersDetails1=this.prettifyGoogleSheetsJSON(data);
      this.showLoader1=false;
    })).subscribe(result => {

    });


  }
  prettifyGoogleSheetsJSON(data)
  {
  for (var i = 0; i < data.feed.entry.length; i++)
  {
      for (var key in data.feed.entry[i])
      {
          if (data.feed.entry[i].hasOwnProperty(key) && key.substr(0,4) === 'gsx$')
          {

              data.feed.entry[i][key.substr(4)] = data.feed.entry[i][key].$t;
              delete data.feed.entry[i][key];
          }
      }
      delete data.feed.entry[i].id;
      delete data.feed.entry[i].updated;
      delete data.feed.entry[i].category;
      delete data.feed.entry[i].content;
      delete data.feed.entry[i].link;
      delete data.feed.entry[i].title;
  }

  return data.feed.entry;
}

  ngOnInit() {

  }






}

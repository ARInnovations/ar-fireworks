import { Component, OnInit } from '@angular/core';
import { ProductDataService } from '../../product-data.service';
import {Router} from "@angular/router";

@Component({
  selector: 'app-product1',
  templateUrl: './product1.component.html',
  styleUrls: ['./product1.component.css']
})
export class Product1Component implements OnInit {

  orderDetails:any;
  orderValue:any;
  constructor(private orderData:ProductDataService,private router: Router) {
    this.orderDetails=orderData.products;
    this.orderValue=orderData.orderValue;

    let index = 1;
    for(let i = 0; i < this.orderDetails.length; i++)
    {
      if(this.orderDetails[i].productCode!='header')
        this.orderDetails[i].index=index++;
    }

  }


  ngOnInit() {
    window.scrollTo(0, 0);
  }

  plusOne(o)
  {
    o.requirement=parseInt(o.requirement)+1;
  }
  minusOne(o)
  {
    o.requirement=parseInt(o.requirement)-1;
    if(o.requirement<0)
      o.requirement=0;
  }
  changeQuantity(val,o)
  {
    o.requirement=val;
    if(val<0)
      o.requirement=0;
  }
  submitOrder()
  {
    this.orderValue[0].orderValue=this.getTotal();
    this.orderValue[0].orderedProduct=1;

    this.router.navigate(['/order/CustomerDetails']);
    
  }
    getTotal()
    {
      let sum = 0;
      for(let i = 0; i < this.orderDetails.length; i++)
      {
        
        if(this.orderDetails[i].productCode!='header')
          sum += this.orderDetails[i].requirement*this.orderDetails[i].rate;
      }
      this.orderValue[0].orderValue=sum;
    return sum;
    }

    getDiscount() {
      return (this.getTotal() * 0.13).toFixed(2)
    }

}

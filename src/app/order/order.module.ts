import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrderComponent } from './order/order.component';
import { OrderRoutingModule } from './order-routing';
import { FormsModule } from '@angular/forms';
import { CustomerDetailsComponent } from './customer-details/customer-details.component';
import { SuccessComponent } from './success/success.component';

import {  ReactiveFormsModule } from '@angular/forms';

import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { Product2Component } from './product2/product2.component';
import { Product1Component } from './product1/product1.component';

@NgModule({
  imports: [
    CommonModule,OrderRoutingModule,FormsModule,HttpClientModule,HttpModule,ReactiveFormsModule
  ],
  declarations: [OrderComponent, CustomerDetailsComponent, SuccessComponent, Product2Component, Product1Component]
})
export class OrderModule { }

import { Component, OnInit } from '@angular/core';
import { ProductDataService } from '../../product-data.service';
import {Router} from "@angular/router";

@Component({
  selector: 'app-success',
  templateUrl: './success.component.html',
  styleUrls: ['./success.component.css']
})
export class SuccessComponent implements OnInit {

  constructor(private orderData:ProductDataService,private router: Router)
  {
    if(orderData.orderValue[0].orderedProduct==0)
    {
      this.router.navigate(['/order']);
    }
  }

  ngOnInit() {
  }

}

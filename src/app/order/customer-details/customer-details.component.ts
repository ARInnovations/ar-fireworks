import { Component, OnInit } from '@angular/core';
import { ProductDataService } from '../../product-data.service';
import {Router} from "@angular/router";
import { map } from 'rxjs/operators';
import { Http, Response, Headers, RequestOptions,HttpModule  } from '@angular/http';
import { FormGroup, FormControl, Validators ,ReactiveFormsModule  } from '@angular/forms';

import {  URLSearchParams } from '@angular/http';
import {Observable} from 'rxjs';


@Component({
  selector: 'app-customer-details',
  templateUrl: './customer-details.component.html',
  styleUrls: ['./customer-details.component.css']
})
export class CustomerDetailsComponent implements OnInit {
  orderDetails:any;
  myObj:any;
  val:any;
  url:any;
  nameVal:any;
  referredbyVal:any;
  nearestVal:any;
  mobileVal:any;
  addressVal:any;
  placeVal:any;
  pincodeVal:any;
  districtVal:any;
  whatsappVal:any;
  emailVal:any;
  orderValue:any;
  stateVal:any;
  states=["Andaman and Nicobar Islands","Andhra Pradesh","Arunachal Pradesh","Assam","Bihar","Chandigarh","Chhattisgarh","Dadra and Nagar Haveli","Daman and Diu","Delhi","Goa","Gujarat","Haryana","Himachal Pradesh","Jammu and Kashmir","Jharkhand","Karnataka","Kerala","Lakshadweep","Madhya Pradesh","Maharashtra","Manipur","Meghalaya","Mizoram","Nagaland","Orissa","Pondicherry","Punjab","Rajasthan","Sikkim","Tamil Nadu","Tripura","Uttar Pradesh","Uttaranchal","West Bengal"];
  origOrderVal:any;
  gstVal:any;
  constructor(private orderData:ProductDataService,private router: Router,private http: Http) {

    this.orderValue=orderData.orderValue;

    if(this.orderValue[0].orderedProduct==0)
    {
      this.router.navigate(['/order']);
    }


    if(this.orderValue[0].orderedProduct==1)
    {
      this.orderDetails=orderData.products;
    }
    else if(this.orderValue[0].orderedProduct==2)
    {
      this.orderDetails=orderData.products2;
    }

  }
  onActivate(e, outlet){
    outlet.scrollTop = 0;
  }

  ngOnInit() {
    window.scrollTo(0, 0);
  }

  customerForm = new FormGroup({
    name: new FormControl('',Validators.required),
    referredby: new FormControl(''),
    email: new FormControl('',[
      Validators.required,Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')]),
    address: new FormControl('',Validators.required),
    place: new FormControl('',Validators.required),
    pincode: new FormControl('',Validators.required),
    district: new FormControl('',Validators.required),
    state: new FormControl('-1',Validators.required),
    whatsapp: new FormControl('',[Validators.required,Validators.pattern('^[6|7|8|9|0][0-9]{9,11}')]),
    mobile: new FormControl('',[Validators.required,Validators.pattern('^[6|7|8|9|0][0-9]{9,11}')]),
    nearest: new FormControl('',Validators.required),
  });

  getGST()
  {
	let disc = ((this.getDiscount() as any));
	let value = (this.orderValue[0].orderValue - disc) *0.18;
    return value;
  }

  getDiscount()
  {
    return (this.orderValue[0].orderValue *0.13).toFixed(2);
  }


  submitData()
  {
    let grandTotal;
    this.nameVal=this.customerForm.get('name').value;
    this.referredbyVal=this.customerForm.get('referredby').value;
    this.emailVal=this.customerForm.get('email').value;
    this.addressVal=this.customerForm.get('address').value;
    this.placeVal=this.customerForm.get('place').value;
    this.pincodeVal=this.customerForm.get('pincode').value;
    this.districtVal=this.customerForm.get('district').value;
    this.stateVal=this.customerForm.get('state').value;
    this.whatsappVal=this.customerForm.get('whatsapp').value;
    this.mobileVal=this.customerForm.get('mobile').value;
    this.nearestVal=this.customerForm.get('nearest').value;
    this.origOrderVal=this.orderValue[0].orderValue;
    this.gstVal=0;
    if(this.stateVal!="Tamil Nadu")
    {
      this.gstVal=this.getGST();
      grandTotal = this.orderValue[0].orderValue + this.getGST() - (this.getDiscount() as any);
    }
    else {
      grandTotal = this.orderValue[0].orderValue - (this.getDiscount() as any);
    }
    let time = new Date().toISOString().split('T')[0];
    this.val="name="+this.nameVal+"&date="+time+"&grandtotal="+grandTotal+"&orderamount="+this.origOrderVal+"&orderamount="+this.origOrderVal+"&discount="+this.getDiscount()+"&gst="+this.gstVal+"&referredby="+this.referredbyVal+"&email="+this.emailVal+"&address="+this.addressVal+"&place="+this.placeVal+"&pincode="+this.pincodeVal+"&district="+this.districtVal+"&state="+this.stateVal+"&whatsapp="+this.whatsappVal+"&mobile="+this.mobileVal+"&nearest="+this.nearestVal;
    let index1=1;
    this.orderDetails.forEach((item,index)=>{
      if(this.orderDetails[index].productCode!='header') {
        this.val+="&p";
        this.val+=(index1++);
        this.val+="=";
        this.val+=this.orderDetails[index].requirement;
      }
    });


    if(this.orderValue[0].orderedProduct==1)
    {
      this.url="https://script.google.com/macros/s/AKfycbypMoEBZPCnu1n-c_7npyEJGZv_HegSblsRH_L-pvHDiUG_0ww/exec?"+  this.val;
    }
    else if(this.orderValue[0].orderedProduct==2)
    {
      this.url="https://script.google.com/macros/s/AKfycbxbHYsDL4tfmndiHVjZETr9oTHIZD06fEKAzXyJGvArswVI_2w/exec?"+  this.val;
    }



      this.http.get(this.url).pipe(map(data => {})).subscribe(result => {});


    this.router.navigate(['/order/success']);
  }

}

import {RouterModule, Routes} from '@angular/router';
import {ModuleWithProviders} from '@angular/core';
import { OrderComponent } from './order/order.component';
import { CustomerDetailsComponent } from './customer-details/customer-details.component';
import { Product2Component } from './product2/product2.component';
import { Product1Component } from './product1/product1.component';

import { SuccessComponent } from './success/success.component';

const orderRoutes: Routes = [
{path:'details',component:OrderComponent},
{path:'CustomerDetails',component:CustomerDetailsComponent},
{path:'success',component:SuccessComponent},
{path:'product1',component:Product1Component},
{path:'product2',component:Product2Component},
{path:'', redirectTo:'details',pathMatch:'full'}
];

export const OrderRoutingModule: ModuleWithProviders = RouterModule.forChild(orderRoutes);
